package ejercicios;

import java.util.Scanner;

public class Ej2 {
	public static void main(String[] args){
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner input ;
		int numeroLeido;
		String cadenaLeida;		
		
		input = new Scanner(System.in);
		
		System.out.println("Introduce un numero ");
		numeroLeido = input.nextInt();
		
		/* el error es debido q que al introducir el numero entero tambien guarda el intro y al pasar a leer la sigiente linea 
		*lee el intro y al no ser un caracter numerico proboca el error
		*/
		
		/*La soluci�n es limpiar el buffer de entrada ejecutando la instrucci�n
		 * input.nextLine(); despu�s de la lectura del carcter numerico
		*/
		
		
		System.out.println("Introduce un numero como String");
		cadenaLeida = input.nextLine();
		
	
		if ( numeroLeido == Integer.parseInt(cadenaLeida)){
			System.out.println("Lo datos introducidos reprensentan el mismo número");
		}
		
		input.close();
		
		
		
	}
}
