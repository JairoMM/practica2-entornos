package ejercicios;

import java.util.Scanner;

public class Ej5 {

	public static void main(String[] args) {
		/*
		 * Ayudate del debugger para entender qué realiza este programa
		 */
		
		
		Scanner lector;
		int numeroLeido;
		int cantidadDivisores = 0;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
		/* En ete programa se intenta saber si un numero es primo o no (significar� que ese n�mero solo es divisible por 1 y por �l mismo). 
		 * El programa lee un n�mero entero introducido por teclado, y desde el 1 hasta ese introducido por teclado 
		 * realiza una serie de divisiones. Al realizarse cada divisi�n, se comprueba que el
		 * resto es 0, y si lo es, se incrementa en uno el contador de divisores. luego se comprueba en el if (con el contador) la cantidad de numeros 
		 * que al dividirlos dan 0, si el contador marca que han habido mas de 2 numeros que al dividirlo por el mismo daba 0 significa que no es primo.
		 * e el resto de los casos si son primos.
		 */
		
		for (int i = 1; i <= numeroLeido; i++) {
			if(numeroLeido % i == 0){
				cantidadDivisores++;
			}
		}
		
		if(cantidadDivisores > 2){
			System.out.println("No lo es");
		}else{
			System.out.println("Si lo es");
		}
		
		
		lector.close();
	}

}
