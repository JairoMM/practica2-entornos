package ejercicios;

import java.util.Scanner;

public class Ej3 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		char caracter=27;
		int posicionCaracter;
		String cadenaLeida, cadenaFinal;
		
		lector = new Scanner(System.in);
		/* El programa intenta localizar la posici�n de nuestro caracter en la cadena introducida. Como no lo localiza, el programa termina.
		 * Una forma de solucionarlo es a�adir la condici�n de que si no lo encuentra, muestre un mensaje diciendo que no
		 * encuentra el caracter en la cadena y volver a pedir un nuevo carcter.
		 */
		
		System.out.println("Introduce un cadena");
		cadenaLeida = lector.nextLine();
		
		posicionCaracter = cadenaLeida.indexOf(caracter);
		
		System.out.println(posicionCaracter);
		
		
		cadenaFinal = cadenaLeida.substring(0, posicionCaracter);
		
		System.out.println(cadenaFinal);
		
		
		lector.close();
	}

}
